﻿using Lab7.AddressControl.Contract;
using Lab7.RemoteImageControl.Contract;
using Lab7.RemoteImageControl.Implementation;
using PK.Container;
using System;
using System.Reflection;
using Container;

namespace Lab7.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new Kontener();

        public static Assembly AddressControlSpec = Assembly.GetAssembly(typeof(IAddress));
        public static Type AddressImpl = typeof(AddressControl.AdresKontrola);
        public static Assembly AddressControlImpl = Assembly.GetAssembly(AddressImpl);

        public static Assembly RemoteImageControlSpec = Assembly.GetAssembly(typeof(IRemoteImage));
        public static Assembly RemoteImageControlImpl = Assembly.GetAssembly(typeof(KontrolaObraz));
        
        #endregion
    }
}
