﻿using Lab7.AddressControl.Contract;
using Lab7.Infrastructure;
using Lab7.RemoteImageControl.Contract;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Lab7.Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            var container = Configuration.ConfigureApp();

            var address = container.Resolve<IAddress>();
            Grid.SetRow(address.Control, 0);
            var rdef = new RowDefinition();
            rdef.Height = new GridLength(30);
            this.Panel.RowDefinitions.Add(rdef);
            this.Panel.Children.Add(address.Control);

            var image = container.Resolve<IRemoteImage>();
            Grid.SetRow(image.Control, 1);
            this.Panel.RowDefinitions.Add(new RowDefinition());
            this.Panel.Children.Add(image.Control);

            this.Panel.ShowGridLines = true;
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }
    }
}
