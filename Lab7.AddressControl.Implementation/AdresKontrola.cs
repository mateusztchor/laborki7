﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab7.AddressControl.Contract;
using System.Windows.Controls;
using System.Windows.Media;
using TinyMVVMHelper;

namespace Lab7.AddressControl
{
    public class AdresKontrola : IAddress
    {
        public event EventHandler<AddressChangedArgs> AddressChanged;

       private Command komenda;
       public Command Komenda
       {
           get { return komenda; }
           set { komenda = value; }
       }

       private string adres;
       public string Adres
       {
           get { return adres; }
           set { adres = value; }
       }
       private Control okno;

       Control IAddress.Control
       {
           get { return okno; }
       }

       public AdresKontrola()
       {
           okno = new Window1();
           this.Komenda = new Command(FireAdressChanged);
       }

       private void FireAdressChanged(object obj)
       {
           if (AddressChanged != null)
           {
               AddressChangedArgs arg = new AddressChangedArgs();
               arg.URL = Adres;
               AddressChanged(this, arg);
           }
       }
    }
}
